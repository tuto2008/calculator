<?php 
	include_once('translate.php');
	$now 					= intval(date("Y"));
	$min 					= $now - 65;
	$max 					= $now - 18;

	$url 					= $_SERVER['DOCUMENT_ROOT'];
	$domainLocation 		= array(
		'C:/xampp/htdocs/MYP/doris' 					=> 'doris',
		'/home2/tuto2008/public_html/ondevelope/doris' 	=> 'doris.ondevelope.com',
		'/home/www-data/mobiliar-rechner.ch' 			=> 'vorsorge-mobiliar.ch',
	);
	//$domain 				= $domainLocation[$url];

	$tagManagerCode			= 'GTM-KXQ5VL'; //test:GTM-529FDT, live:GTM-KXQ5VL, Luis:GTM-TSN59B3

	$langs 					= array('de','fr','it');
	$lang 					= isset($_GET["lang"]) && in_array($_GET["lang"],$langs) ? $_GET["lang"] : $langs[0];
	//$t_domain 				= isset($_GET["domain"]) ? $_GET["domain"] : '*';
	$t_domain 				= isset($_GET["domain"]) ? $_GET["domain"] : $domainLocation[$url];
	$ts 					= new Translate($lang);

	$resultTexts 			= array(
		'sparbetrag' 		=> array(
			$ts->tsr('title_sparbetrag'),  
			$ts->tsr('result_text2'),
			$ts->tsr('result_text3'),
			$ts->tsr('result_text4')
		),
		'altersrente' 		=> array(
			$ts->tsr('title_altersrente'),
			$ts->tsr('result_text2'),
			$ts->tsr('result_text3'),
			$ts->tsr('result_text4')
		)
	);

	$resultTexts = json_encode($resultTexts);
?>