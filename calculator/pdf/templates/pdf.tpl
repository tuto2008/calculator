<page backtop="0" backbottom="0" backleft="0" backright="0">
	<style type="text/Css">
	<!--
		html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}table{border-collapse:collapse;border-spacing:0}
		img.background{width:100%;height:auto;position:absolute;z-index:1}
		.content{width:100%;height:auto;position:absolute;z-index:2}
		.content div{position:absolute;font-family:'mobiliar'}
		.template{position:absolute;}
		.titleimg{left:204mm;top:13mm;width:80mm;}
		.type{display:table-cell;vertical-align:middle;left:16.5mm;top:49.5mm;width:37mm;font-size:4.2mm;height:18mm;text-align:center;}
		.part{color:#55575a;display:table-cell;vertical-align:middle;top:49.5mm;font-size:2.2mm;height:18mm}
		.part strong{font-size:150%;width:100%;color:#000000;font-weight:normal}
		.monthly_income{left:67.5mm;width:37mm}
		.monthly_rents{left:143mm;width:34mm}
		.pension_age{left:219.5mm;width:34mm}
		#part_01{width:37.8mm;height:17.5mm;background:#70c3d4;top:49.6mm;left:16mm}
		#part_02{width:228mm;height:17.5mm;top:49.6mm;left:56.3mm;background:#f2f1ed}
		.values{color:#55575a;display:table-cell;vertical-align:middle;top:53mm;font-size:3.3mm;height:11mm;width:27.5mm;text-align:right;padding-right:8.5mm}
		.values strong{font-family:'mobiliar_bold';font-weight:bold;font-size:130%;color:#e62525;margin-left:2mm}
		#val_01{left:97.5mm}
		#val_02{left:174.5mm}
		#val_03{left:242.5mm;padding-right:0}
		.currency{border-right:1px solid #d5d3d0}
		.logo{position:absolute;width:31mm;height:auto;left:16.4mm}
		.result{top:103.5mm;width:40mm;height:50mm;text-align:center}
		.result h3{font-weight:normal;font-size:3.3mm;line-height:130%}
		.result p{color:#fff;font-weight:bold}
		.circle p{position:absolute;top:27mm}
		.result strong{font-size:170%;font-weight:normal;font-family:'mobiliar'}
		#bullet{left:49.3mm}
		#circle1{left:104.4mm}
		#circle2{left:156.2mm}
		#circle3{left:208.2mm}
		#bullet h3{margin-top:22mm;margin-bottom:1.8mm;line-height:130%;color:#fff;}
		.disclaimer{top:176mm;left:16mm;font-size:3.3mm;width:266mm;color:#000000}
		.disclaimer p{margin-bottom:2.5mm}
		.disclaimer a{color:#000000;text-decoration:none}
	-->
	</style>

	<!-- Template items -->
	<img class="background" src="{img_path}background.gif" />
	<div id="part_01" class="template"></div>
	<div id="part_02" class="template"></div>
	<img class="logo" src="{img_path}logo_{template_lang}.gif" />
	<img class="titleimg template" src="{img_path}title_{template_lang}.gif" />
	<!-- END Template items -->

	

	<div class="content">

		<!-- Data -->
		<div class="type">{template_type}</div>
		<!-- END Data -->

		<!-- Inputs -->
		<div class="monthly_income part">
			<strong>{template_val1_title}</strong>
			<br>{template_val1_text}
		</div>
		<div class="monthly_rents part">
			<strong>{template_val2_title}</strong>
			<br>{template_val2_text}
		</div>
		<div class="pension_age part">
			<strong>{template_val3_title}</strong>
			<br>{template_val3_text}
		</div>
		<div id="val_01" class="values currency">
			{template_currency} <strong>{data_val1}</strong>
		</div>
		<div id="val_02" class="values currency">
			{template_currency} <strong>{data_val2}</strong>
		</div>
		<div id="val_03" class="values">
			<strong>{data_val3}</strong> {template_age}
		</div>
		<!-- END Inputs -->

		<!-- Results -->
		<div id="bullet" class="result">
			<h3>{template_result_text1}</h3>
			<p>{template_currency} <strong>{data_val4}</strong></p>
		</div>
		<div id="circle1" class="result circle">
			<h3>{template_result_text2}</h3>
			<p>{template_currency} <strong>{data_val5}</strong></p>
		</div>
		<div id="circle2" class="result circle">
			<h3>{template_result_text3}</h3>
			<p>{template_currency} <strong>{data_val6}</strong></p>
		</div>
		<div id="circle3" class="result circle">
			<h3>{template_result_text4}</h3>
			<p>{template_currency} <strong>{data_val7}</strong></p>
		</div>
		<!-- END Results -->

		<!-- Disclaimer -->
		<div class="disclaimer template">
			<p>{template_disclaimer_text}</p>
			<p>{template_disclaimer_address}</p>
		</div>
		<!-- END Disclaimer -->
	</div>
</page>