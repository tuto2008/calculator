<?php 
	include_once('../translate.php');

	define('BASE_URL', dirname(__FILE__)."/");
   	$img_path 		= BASE_URL.'assets/images/';

   	$typeValues 	= array('altersrente','sparbetrag');
   	$type 			= isset($_REQUEST['type']) && in_array($_REQUEST['type'],$typeValues) ? $_REQUEST['type'] : $typeValues[0];

   	if( !isset($_REQUEST['values']) || $_REQUEST['values'] == ''){
   		//Checking if values has been sent by $_POST
   		die();
   	} else {
   		$values 		= (array) json_decode($_REQUEST['values']);
   		if(count($values) != 7){
   			//Checking if values count is 7 (number of elements required)
   			die();
   		}
   	}

	$lang = $_REQUEST['lang'];
	$ts = new Translate($lang);

    $data = array(
        'template'     				=> array(
            'currency'				=> $ts->tsr('currency'),
            'type'     				=> ucfirst($type),
            'val1_title'  			=> $ts->tsr('monthly_income'),
            'val1_text'  			=> $ts->tsr('gross'),
            'val3_title'  			=> $ts->tsr('pension_age'),
            'val3_text'  			=> $ts->tsr('pre_post_retirement'),
            'age'  					=> $ts->tsr('PDF_age'),
            'result_text2'			=> $ts->tsr('result_text2'),
            'result_text3'			=> $ts->tsr('result_text3'),
            'result_text4'			=> $ts->tsr('result_text4'),
            'disclaimer_text'		=> $ts->tsr('disclaimer_pdf'),
            'disclaimer_address'	=> $ts->tsr('disclaimer_address'),
            'lang'					=> $lang
        )
    );

    $data['data'] = $values;

    $types 					= array(
    	'altersrente' 		=> array(
		    'val2_title'  	=> $ts->tsr('PDF_altersrente_val2_title'),
		    'val2_text'  	=> $ts->tsr('net'),
		    'result_text1'	=> $ts->tsr('title_altersrente')
    	),
    	'sparbetrag' 		=> array(
		    'val2_title'  	=> $ts->tsr('savings_potential'),				
		    'val2_text'  	=> $ts->tsr('end_month'),
		    'result_text1'	=> $ts->tsr('title_sparbetrag')
    	),
    );

    $data['template']+=$types[$type];

	class PdfGenerator {
		var $data;
	   	var $template;

		function PdfGenerator($data,$template){
			$this->tpl_path  	= BASE_URL.'templates/';
			$this->img_path  	= BASE_URL.'assets/images/';
			$this->data       	= $data;
			$this->template   	= file_get_contents($this->tpl_path.$template);
			$this->Fill_template();
			$this->Output();
		}

		//get template and replace variables
		private function Fill_template(){
			$changes  = array('{img_path}'=> $this->img_path );
			foreach($this->data as $type=>$values){
				foreach($values as $key=>$value){
					$changes["{".$type."_".$key."}"] = $value;
				}
			} 
			//replace all tags in template
			$this->template = strtr($this->template,$changes);
		}

		//pdf generation
		private function Output(){
			require_once('html2pdf/vendor/autoload.php');
			$html2pdf = new HTML2PDF('L','A4','es', true, 'UTF-8', array(0, 0, 0, 0));
			$html2pdf->AddFont('mobiliar', 'normal', 'assets/fonts/mobiliar.php');
			$html2pdf->AddFont('mobiliar', 'bold', 'assets/fonts/mobiliar_bold.php');
			$html2pdf->setDefaultFont('mobiliar');
			$html2pdf->WriteHTML($this->template);
			$html2pdf->Output();
		}
	}
?>