<?php 
	include_once('config.php');
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
	<meta charset="UTF-8">
	<title><?php $ts->ts('title_html'); ?></title>
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link type="text/css" rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/themes/smoothness/jquery-ui.css"> 
	<link type="text/css" rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css">
	<link type="text/css" rel="stylesheet" href="assets/css/style.css?v=3">

	<script type="text/javascript">
		var TDOMAIN = '<?php echo $t_domain; ?>';
	</script>

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.custom-select.js"></script>
	<script type="text/javascript" src="assets/js/jquery.animateSprite.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/pixi.min.js"></script>
	<script type="text/javascript" src="assets/js/operations.js"></script>
	<script type="text/javascript" src="assets/js/animation.js?v=3"></script>
	<script type="text/javascript" src="assets/js/main.js?v=3"></script>

	<script>
		var resultTexts = <?php echo $resultTexts; ?>;
		var currentCurrency = '<?php echo $ts->ts('currency'); ?>';
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','<?php echo $tagManagerCode; ?>');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $tagManagerCode; ?>"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="wrapHeight">
		<div id="calculatorContent" class="step_01">
			<form action="" method="post" id="calculatorForm">
				<span class="pages"></span>
				<div id="step_01" class="block">
					<div class="wrapCols">
						<div class="col_01">
							<ul class="gender">
								<li><input type="radio" name="gender" value="MALE"><?php $ts->ts('gender_male'); ?></li>
								<li><input type="radio" name="gender" value="FEMALE"><?php $ts->ts('gender_female'); ?></li>
							</ul>
						</div>
						<div class="col_02">
							<label for="year" class="yearLabel">
								<strong class="label"><?php $ts->ts('year'); ?></strong>
								<?php 
								/*
									$years = array();
									for($x=$min;$x<=$max;$x++){ 
										$years[] = $x;
									} 
									$years = implode(',',$years);
									*/
								?>
								<div class="year2" data-items="<?php// echo $years; ?>"></div>
								<input type="text" id="yearField" value="1952" data-min="<?php echo $min; ?>"  data-max="<?php echo $max; ?>" class="rangenumber field">
							</label>
						</div>
						<a href="#step_02" id="btnStart" class="btn disable"><?php $ts->ts('btn_start'); ?></a>
					</div>
					<div class="disclaimer">
						<p><?php $ts->ts('disclaimer_intro'); ?></p>
					</div>
				</div>
				<div id="step_02" class="block">
					<div id="options" class="col_01">
						<div class="typesContent">
							<nav class="type">
								<a href="#type_01" data-type="sparbetrag" class="option_01 active"><?php $ts->ts('savingsTab'); ?></a>
								<a href="#type_02" data-type="altersrente" class="option_02"><?php $ts->ts('retirementTab'); ?></a>
							</nav>
							<div id="type_01" class="typeTab active">
								<div id="monatseinkommen" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('monthly_income'); ?></strong> <?php $ts->ts('gross'); ?></h4>
										<label class="inputField" for="monatseinkommen">
											<input id="monthlyNetIncome1" type="text" maxlength="5" class="field number monatseinkommen required" data-max="20000" name="monatseinkommen" value="4&rsquo;200">
											<span><?php $ts->ts('currency'); ?></span>
										</label>
									</div>
								</div>
								<div id="einsparung" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('savings_potential'); ?></strong> <?php $ts->ts('end_month'); ?></h4>
										<label class="inputField" for="einsparung"><!-- class="sliderWrap" -->
											<!--<div class="slider" data-range="min" data-min="0" data-max="5000" data-step="10" data-value="250"></div>
											<input id="monthlySavings" type="text" readonly name="einsparung" class="sliderValue currency">-->
											<input id="monthlySavings" type="text" data-type="deutch" data-min="0" data-max="5000" name="einsparung" class="sliderValue currency rangenumber field" value="250">
											<span><?php $ts->ts('currency'); ?></span>
										</label>
									</div>
								</div>
								<div id="pensionalter" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('pension_age'); ?></strong> <?php $ts->ts('pre_post_retirement'); ?></h4>
										<label for="pensionalter"><!-- class="sliderWrap" -->
											<!--<div class="slider" data-range="min" data-min="63" data-max="70" data-value="65"></div>
											<input id="startPensionAge1" type="text" readonly name="pensionalter" class="sliderValue pensionalter">-->
											<input id="startPensionAge1" type="text" data-min="63" data-max="70" name="pensionalter" class="sliderValue pensionalter rangenumber field" value="65">
										</label>
									</div>
								</div>
							</div>
							<div id="type_02" class="typeTab">
								<div id="monatseinkommen" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('monthly_income'); ?></strong> <?php $ts->ts('gross'); ?></h4>
										<label class="inputField" for="monatseinkommen">
											<input id="monthlyNetIncome2" type="text" maxlength="6" class="field number monatseinkommen required" data-max="20000" name="monatseinkommen" value="4&rsquo;200">
											<span><?php $ts->ts('currency'); ?></span>
										</label>
									</div>
								</div>
								<div id="kunftige" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('PDF_altersrente_val2_title'); ?></strong> netto</h4>
										<label class="inputField" for="kunftige">
											<input id="monatsrente" type="text" maxlength="7" class="field number required" name="kunftige" value="">
											<span><?php $ts->ts('currency'); ?></span>
										</label>
									</div>
								</div>
								<div id="pensionalter" class="contentBlock">
									<div class="wrap">
										<h4><strong><?php $ts->ts('pension_age'); ?></strong> <?php $ts->ts('pre_post_retirement'); ?></h4>
										<label for="pensionalter"><!-- class="sliderWrap" -->
											<!--<div class="slider" data-range="min" data-min="63" data-max="70" data-value="65"></div>
											<input id="startPensionAge2" type="text" readonly name="pensionalter" class="sliderValue pensionalter">-->
											<input id="startPensionAge2" type="text" data-min="63" data-max="70" name="pensionalter" class="sliderValue pensionalter rangenumber field" value="65">
										</label>
									</div>
								</div>
							</div>
							<a href="#" id="btnCalculate" class="btn"><?php $ts->ts('btn_calculate'); ?></a>
						</div>
						<div class="actionContent">
							<a href="#" id="btnTypes" class="btn"><?php $ts->ts('btn_change'); ?></a>
							<a href="#" id="btnReset" class="btn btn01"><?php $ts->ts('btn_reset'); ?></a>
							<a href="#" id="btnPDF" class="btn btn02 disable"><?php $ts->ts('btn_createPDF'); ?></a>
						</div>
					</div>
					<div id="animationWrap">

						<div class="ballAnimation">
							<div id="sphere-container"></div>
							<div class="shadow"></div>
						</div>

						<div class="aniResults">
							<div class="wrapSprite">
								<div id="bullet" class="sprite"></div>
							</div>
							<div class="wrapSprite">
								<div id="ball1" class="sprite ball"></div>
							</div>
							<div class="wrapSprite">
								<div id="ball2" class="sprite ball"></div>
							</div>
							<div class="wrapSprite">
								<div id="ball3" class="sprite ball"></div>
							</div>
						</div>
					</div>
					<div class="disclaimer">
						<p><?php $ts->ts('disclaimer_text'); ?></p>
					</div>
				</div>
			</form>
			<form id="pdfGenerator" name="pdfGenerator" action="pdf/index.php" method="post" target="_blank">
				<input type="hidden" id="resultType" name="type" value="">
				<input type="hidden" id="lang" name="lang" value="<?php echo $lang; ?>">
				<input type="hidden" id="resultValues" name="values" value="">
			</form>
		</div>
		<input type="hidden" name="domain" id="domain" value="<?php echo $domain; ?>">
	</div>
</body>
</html>