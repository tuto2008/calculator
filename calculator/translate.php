<?php 
	class Translate { 
	    public $lang = ''; 
	    public $tags = array(); 
	    
	    function __construct($lang) {
	    	global $tags;
	        $this->tags = $tags;
	        $this->lang = $lang;
	    } 

	    public function ts($tag){
	    	/* print the translate on demand */
	    	echo $this->tags[$tag][$this->lang];
	    }

	    public function tsr($tag){
	    	/* return the translate to use in a variable */
	    	return $this->tags[$tag][$this->lang];
	    }
	} 

	$tags = array(
		'title_html' 					=> array(
			'de' => 'Calculator',
			'fr' => 'Calculateur',
			'it' => 'Calcolatore'
		),
		'gender_male' 					=> array(
			'de' => 'Mann',
			'fr' => 'Homme',
			'it' => 'Uomo'
		),
		'gender_female' 				=> array(
			'de' => 'Frau',
			'fr' => 'Femme',
			'it' => 'Donna'
		),
		'year' 							=> array(
			'de' => 'Jahrgang',
			'fr' => 'Année de naissance',
			'it' => 'Anno di nascita'
		),
		'btn_start' 					=> array(
			'de' => 'Start',
			'fr' => 'Début',
			'it' => 'Avvio'
		),
		'savingsTab'					=> array(
			'de' => 'Sparbetrag',
			'fr' => 'Epargne',
			'it' => 'Importo di risparmio'
		),
		'retirementTab'					=> array(
			'de' => 'Altersrente',
			'fr' => 'Rente de vieillesse',
			'it' => 'Rendita di vecchiaia'
		),
		'monthly_income'				=> array(
			'de' => 'Monatseinkommen',
			'fr' => 'Revenu mensuel',
			'it' => 'Reddito mensile'
		),
		'net'							=> array(
			'de' => 'netto',
			'fr' => 'net',
			'it' => 'rete'
		),
		'gross'							=> array(
			'de' => 'brutto',
			'fr' => 'brut',
			'it' => 'lordo'
		),
		'currency'						=> array(
			'de' => 'CHF',
			'fr' => 'CHF',
			'it' => 'CHF'
		),
		'savings_potential'				=> array(
			'de' => 'Mögliches Sparpotential',
			'fr' => 'Potentiel d’épargne éventuel',
			'it' => 'Possibile potenziale di risparmio'
		),
		'end_month'						=> array(
			'de' => 'monatlich',
			'fr' => 'mensuel',
			'it' => 'mensile'
		),
		'pension_age'					=> array(
			'de' => 'Pensionsalter',
			'fr' => 'Age de la retraite',
			'it' => 'Età di pensionamento'
		),
		'pre_post_retirement'			=> array(
			'de' => 'Vor/Nachzeitige Pensionierung',
			'fr' => 'Retraite anticipée / différée',
			'it' => 'Pensionamento anticipato/posticipato'
		),
		'btn_calculate'					=> array(
			'de' => 'Berechnen',
			'fr' => 'Calculer',
			'it' => 'Calcolare'
		),
		'btn_reset'						=> array(
			'de' => 'Reset',
			'fr' => 'Réinitialiser',
			'it' => 'Reset'
		),
		'btn_change'					=> array(
			'de' => 'Ändern',
			'fr' => 'Modifier',
			'it' => 'Modificare'
		),
		'btn_createPDF'					=> array(
			'de' => 'PDF generieren',
			'fr' => 'Générer un fichier PDF',
			'it' => 'Generare PDF'
		),
        'title_sparbetrag'				=> array(
			'de' => 'Geschätzte künftige <br>Monatsrente',
			'fr' => 'Rente mensuelle <br>future estimée',
			'it' => 'Rendita mensile <br>futura stimata'
		),
        'title_altersrente'				=> array(
			'de' => 'Monatlicher <br>Sparbetrag',
			'fr' => 'Montant d’épargne <br>par mois',
			'it' => 'Importo mensile <br>di risparmio'
		),
        'result_text2'					=> array(
			'de' => 'AHV Betrag pro Monat',
			'fr' => 'Montant AVS par mois',
			'it' => 'Contributo AVS mensile'
		),
        'result_text3'					=> array(
			'de' => 'Berechneter statistischer <br>Wert nach BVG',
			'fr' => 'Valeur statistique calculée sur la base de la LPP',
			'it' => 'Valore statistico <br>calcolato secondo la LPP'
		),
        'result_text4'					=> array(
			'de' => 'Berechnete Rente aus <br>Vorsorgekapital',
			'fr' => 'Rente calculée à partir <br>du capital de prévoyance',
			'it' => 'Rendita calcolata dal <br>capitale previdenziale'
		),
		'disclaimer_intro' 				=> array(
			'de' => '*Die Rentenschätzung basiert auf einem vereinfachten Berechnungsverfahren. Da einzig auf hypothetische Werte abgestimmt wird, kann es sich bei der Rentenleistung ebenfalls nur um einen hypothetischen Wert handeln. Die Rentenschätzung hat somit nur einen unverbindlichen Charakter. Für eine individuelle Berechnung Ihrer Rente nehmen Sie bitte mit einem unserer Vorsorgeexperten Kontakt auf.',
			'fr' => '*L’estimation des rentes est obtenue par le biais d’une procédure simplifiée et ne représente aucune garantie. La présence d’éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n’ont par conséquent aucune valeur juridique et n’engagent en aucun cas la Mobilière. Pour un calcul individualisé, nous vous invitons à prendre contact avec l’un de nos experts en prévoyance.',
			'it' => '*La stima della rendita si basa su un procedimento di calcolo semplificato. Poiché ci si basa su valori puramente ipotetici, anche il valore della rendita calcolata è ipotetico. Pertanto la stima della rendita ha carattere indicativo.  Per avere un calcolo individuale della rendita, mettetevi in contatto con un nostro esperto di previdenza.'
		),
        'disclaimer_text' 				=> array(
			'de' => '*Die Rentenschätzung basiert auf einem vereinfachten Berechnungsverfahren. Da einzig auf hypothetische Werte abgestimmt wird, kann es sich bei der Rentenleistung ebenfalls nur um einen hypothetischen Wert handeln. Die Rentenschätzung hat somit nur einen unverbindlichen Charakter. Für eine individuelle Berechnung Ihrer Rente nehmen Sie bitte mit einem unserer Vorsorgeexperten Kontakt auf.',
			'fr' => '*L’estimation des rentes est obtenue par le biais d’une procédure simplifiée et ne représente aucune garantie. La présence d’éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n’ont par conséquent aucune valeur juridique et n’engagent en aucun cas la Mobilière. Pour un calcul individualisé, nous vous invitons à prendre contact avec l’un de nos experts en prévoyance.',
			'it' => '*La stima della rendita si basa su un procedimento di calcolo semplificato. Poiché ci si basa su valori puramente ipotetici, anche il valore della rendita calcolata è ipotetico. Pertanto la stima della rendita ha carattere indicativo.  Per avere un calcolo individuale della rendita, mettetevi in contatto con un nostro esperto di previdenza.'
		),

		/* PDF template generator */
        'PDF_age'  						=> array(
			'de' => 'Jahre',
			'fr' => 'Ans',
			'it' => 'Anni'
		),
        'PDF_altersrente_val2_title'	=> array(
			'de' => 'Künftige Monatsrente',
			'fr' => 'Rente mensuelle future',
			'it' => 'futura rendita mensile'
		),
        'disclaimer_pdf' 				=> array(
			'de' => '*Die Rentenschätzung basiert auf einem vereinfachten Berechnungsverfahren. Da einzig auf hypothetische Werte abgestimmt wird, kann es sich bei der Rentenleistung ebenfalls nur um einen hypothetischen Wert handeln. Die Rentenschätzung hat somit nur einen unverbindlichen Charakter. Für eine individuelle Berechnung Ihrer Rente nehmen Sie bitte mit einem unserer Vorsorgeexperten Kontakt auf.',
			'fr' => '*L’estimation des rentes est obtenue par le biais d’une procédure simplifiée et ne représente aucune garantie. La présence d’éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n’ont par conséquent aucune valeur juridique et n’engagent en aucun cas la Mobilière. Pour un calcul individualisé, nous vous invitons à prendre contact avec l’un de nos experts en prévoyance.',
			'it' => '*La stima della rendita si basa su un procedimento di calcolo semplificato. Poiché ci si basa su valori puramente ipotetici, anche il valore della rendita calcolata è ipotetico. Pertanto la stima della rendita ha carattere indicativo.  Per avere un calcolo individuale della rendita, mettetevi in contatto con un nostro esperto di previdenza.'
		),
        'disclaimer_address'			=> array(
			'de' => 'Schweizerische Mobiliar Lebensversicherungs-Gesellschaft AG, Kundendienst / Private Vorsorge - Telefon: 022 363 93 93 - E-Mail: <a href="mailto:kundendienst@mobiliar.ch" target="_blank">kundendienst@mobiliar.ch</a>',
			'fr' => 'Mobilière Suisse Société d’assurances sur la vie SA, Service Clients / Prévoyance privée - T 022 363 96 96 - <a href="mailto:serviceclients@mobiliere.ch" target="_blank">serviceclients@mobiliere.ch</a>',
			'it' => 'Mobiliare Svizzera Società d’assicurazioni sulla vita SA, Servizio Clienti / Previdenza privata - T 022 393 97 97 - <a href="mailto:servizioclienti@mobiliare.ch" target="_blank">servizioclienti@mobiliare.ch</a>'
		),
        /* END PDF */
	);
?>