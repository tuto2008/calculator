(function() {

var Calculator = function() {

	//Calculated values
	this.values = {	
		//Needs to be set --- DEMO DATA!!!

		//For All Calculations
		age: 66,
		sex: Calculator.MALE,
		startPensionAge: 65,
		monthlyBrutIncome: 6000,
		//monthlyNetIncome: 6000,
		monthlyNIncomes: 13,

		//For From Savings Calculation
		monthlySavings: 250,

		//For Wished Calculation
		wishedPension: 4615.883294109986,

		//Internal variables

		interest_savings: 0.01, //% interest of monthly payments
		interest_pension: 0.01, //% interest to calculate the pension from savings
		//Umwandlungssatz ordentlich => Es el porcentaje para calcular la pensión en la edad „normal“ para la jubilación (por el momento siempre con 65). Es decir, si el capital es de 100’000 la pensión sería 6’800.
		commonRate: 0.068,
		commonStartPensionAge: 65, //%Normales Pensionsalter
		endPensionYears: 90,//% Annahme, dass das Kapital bis 90 verbraucht wird
		AHV_IV: 0.1025,//% AHV/IV Beiträge
		ALVmaxGrantedIncome: 148200,//% max. versichertes ALV Einkommen
		ALVmax: 0.022,//% ALV Beiträge bis 148'000 Einkommen
		ALVmin: 0.01,//%  ALV Beiträge den 148'000 übersteigendes Einkommen

		//yearlyNetIncome: null,
		yearlyBrutIncome: null,
		socialInsuranceDeductions: null, //%
		savingYears: null,
		expectedCapitalFromSavings: null,
		totalPaidAmount: null,
		ALV: null, //%
		//incomeNetToBrute: null, //%
		insuredIncome: null,
		//insuredIncomeSim: null, //% NEW

		//Beitrag Arbeitnehmer => es el porcentaje que tiene que pagar el empleado de su sueldo brutto (de su nomina)
		//netIncomeRatePayment: null,
		//Umwandlungssatz bei Pensionierung: Es el porcentaje para calcular la pensión en la edad „deseada“ para la jubilación (entre 63 y 70).
		targetRate: null,

		//min.vers.lohn = Sueldo mínimo asegurado
		minGrantedIncome: 21150,
		//Koord.abzug = deducción coordinada
		coordinatedDeduction: 24675,
		//Koord.abzug = deducción coordinada
		minCoordinatedIncome: 3525,
		//max.vers.Lohn = Sueldo máximo asegurado
		maxGrantedIncome: 84600,
		//max.kord.Lohn = Sueldo coordinado máximo
		maxCoordinatedIncome: null,

		maxPension: 2350,
		minPension: null,
		skala44Value: null,

		pensionFromIncomeYearly: null,
		pensionFromIncome: null,
		pensionFromSavings: null,
		pensionFromState: null,
		pensionMonthlyIncome: null,
		pensionDifference: null,
		pensionYears: null,

		//Added for Wished Calculation
		requiredCapitalForPensionFromSavings: null

	};

	this.contributionRates = [
		{years: 0, value: 0.00},
		{years: 25, value: 0.07},
		{years: 35, value: 0.10},
		{years: 45, value: 0.15},
		{years: 55, value: 0.18}
	];

	this.pensionsFromStateRates = [
		{years:63, value: -0.136},
		{years:64, value: -0.068},
		{years:65, value: 0},
		{years:66, value: 0.052},
		{years:67, value: 0.108},
		{years:68, value: 0.171},
		{years:69, value: 0.24},
		{years:70, value: 0.315}
	];

	this.skala44 = [
		{income:14100, pension: 1175},
		{income:15510, pension: 1206},
		{income:16920, pension: 1236},
		{income:18330, pension: 1267},
		{income:19740, pension: 1297},
		{income:21150, pension: 1328},
		{income:22560, pension: 1358},
		{income:23970, pension: 1389},
		{income:25380, pension: 1419},
		{income:26790, pension: 1450},
		{income:28200, pension: 1481},
		{income:29610, pension: 1511},
		{income:31020, pension: 1542},
		{income:32430, pension: 1572},
		{income:33840, pension: 1603},
		{income:35250, pension: 1633},
		{income:36660, pension: 1664},
		{income:38070, pension: 1694},
		{income:39480, pension: 1725},
		{income:40890, pension: 1755},
		{income:42300, pension: 1786},
		{income:43710, pension: 1805},
		{income:45120, pension: 1824},
		{income:46530, pension: 1842},
		{income:47940, pension: 1861},
		{income:49350, pension: 1880},
		{income:50760, pension: 1899},
		{income:52170, pension: 1918},
		{income:53580, pension: 1936},
		{income:54990, pension: 1955},
		{income:56400, pension: 1974},
		{income:57810, pension: 1993},
		{income:59220, pension: 2012},
		{income:60630, pension: 2030},
		{income:62040, pension: 2049},
		{income:63450, pension: 2068},
		{income:64860, pension: 2087},
		{income:66270, pension: 2106},
		{income:67680, pension: 2124},
		{income:69090, pension: 2143},
		{income:70500, pension: 2162},
		{income:71910, pension: 2181},
		{income:73320, pension: 2200},
		{income:74730, pension: 2218},
		{income:76140, pension: 2237},
		{income:77550, pension: 2256},
		{income:78960, pension: 2275},
		{income:80370, pension: 2294},
		{income:81780, pension: 2312},
		{income:83190, pension: 2331},
		{income:84600, pension: 2350}
	];

};

Calculator.MALE = 'male';
Calculator.FEMALE = 'female';

Calculator.prototype = {

	getFromSavings: function() {

		var v = this.values;

		/*v.remainingWorkingYears = v.startPensionAge - v.age;
		v.totalWorkingYears = v.startPensionAge - v.age;*/
		v.yearlyBrutIncome = v.monthlyBrutIncome * v.monthlyNIncomes;
		//NEW
		//v.insuredIncomeSim = v.yearlyNetIncome > v.minGrantedIncome ? Math.min(Math.max(v.yearlyNetIncome - v.coordinatedDeduction, v.minCoordinatedIncome), (v.maxGrantedIncome - v.coordinatedDeduction)) : 0;
		//ENDNEW
		
		//v.netIncomeRatePayment = this.getContributionRate(v.age).value * 0.5;
		v.targetRate = v.commonRate + (v.startPensionAge - v.commonStartPensionAge) * 0.002;

		//v.ALV = (v.yearlyNetIncome < v.ALVmaxGrantedIncome) ? v.ALVmax : (v.ALVmaxGrantedIncome * v.ALVmax + (v.yearlyNetIncome - v.ALVmaxGrantedIncome) * v.ALVmin) / v.yearlyNetIncome;

		//v.incomeNetToBrute = (v.AHV_IV + v.ALV) * 0.5;

		//NEW
		/*v.socialInsuranceDeductions = v.netIncomeRatePayment + v.incomeNetToBrute;*/
		//v.socialInsuranceDeductions = v.netIncomeRatePayment * (v.insuredIncomeSim / v.yearlyNetIncome) + v.incomeNetToBrute;
		//ENDNEW
		//v.yearlyBrutIncome = v.yearlyNetIncome / (1 - v.socialInsuranceDeductions);

		v.savingYears = v.startPensionAge - v.age;
		v.pensionYears = v.endPensionYears - v.startPensionAge;
		v.totalPaidAmount = v.monthlySavings * 12 * v.savingYears;

		v.maxCoordinatedIncome = v.maxGrantedIncome - v.coordinatedDeduction;

		v.insuredIncome = v.yearlyBrutIncome > v.minGrantedIncome ? Math.min(Math.max(v.yearlyBrutIncome - v.coordinatedDeduction, v.minCoordinatedIncome), v.maxCoordinatedIncome) : 0;

		//Lets calculate N28 cell
		var sum = 0;
		for (var i = 0; i < this.contributionRates.length - 1; ++i) {
			sum += this.contributionRates[i].value * v.insuredIncome * 10;
		}
		//Last one has a special treatment
		sum += (this.contributionRates[this.contributionRates.length - 1].value * v.insuredIncome) * Math.min((10 + v.startPensionAge - v.commonStartPensionAge), 10);

		v.pensionFromIncomeYearly = sum * v.targetRate;

		v.pensionFromIncome = Math.round(v.pensionFromIncomeYearly / 12);

		/*Age correction BVG */
		if (v.age>=50)
			v.pensionFromIncome = v.pensionFromIncome * 0.95;

		v.minPension = v.maxPension / 12;

		/*Age correction AHV */
		if (v.age >= 55) {
        	v.skala44Value = this.getSkala44(v.yearlyBrutIncome * 0.88).pension; //12% Reduktion des Durschnittseinkommens ab 55 Jahren
    	} else if (v.age >= 45) {
        	v.skala44Value = this.getSkala44(v.yearlyBrutIncome * 0.94).pension; //6% Reduktion des Durschnittseinkommens von 45-54 Jahren
    	} else {
        	v.skala44Value = this.getSkala44(v.yearlyBrutIncome).pension; //kein Abzug des Durschnittseinkommens unter 45 Jahren
    	}

		//v.skala44Value = this.getSkala44(v.yearlyBrutIncome).pension;


		v.pensionFromState = v.skala44Value * (1 + this.getPensionFromStateRates(v.startPensionAge).value);

		v.pensionDifference = v.monthlyBrutIncome - v.pensionMonthlyIncome;
		
		v.expectedCapitalFromSavings = this.expectedCapitalFromSavings(
			v.interest_savings/12,
			v.savingYears * 12,
			-v.monthlySavings,
			0,
			1);

		if (v.age > v.startPensionAge) {
			v.pensionFromSavings = 0;
		} else {		
			v.pensionFromSavings = -this.pensionFromSavings(
				v.interest_pension / 12,
				12 * v.pensionYears,
				v.expectedCapitalFromSavings,
				0,
				0);
		}

		v.pensionMonthlyIncome = v.pensionFromSavings + v.pensionFromIncome + v.pensionFromState;

		return {
			monthlyBrutIncome: v.monthlyBrutIncome,
			fromSavings: v.pensionFromSavings,
			fromIncome: v.pensionFromIncome,
			fromState: v.pensionFromState,
			monthlyIncome: v.pensionMonthlyIncome,
			difference: v.pensionDifference
		};
	},

	getWished: function() {

		var v = this.values;

		this.getFromSavings();

		v.missingIncome = v.wishedPension - (v.pensionFromState + v.pensionFromIncome);

		v.requiredCapitalForPensionFromSavings = this.requiredCapitalForPensionFromSavings(
			v.interest_savings / 12,
			12 * v.pensionYears,
			v.missingIncome,
			0,
			0
		);

		if (v.age > v.startPensionAge) {
			v.requiredSavingsForPensionFromSavings = 0;
		} else {		
			v.requiredSavingsForPensionFromSavings = this.requiredSavingsForPensionFromSavings(
				v.interest_pension / 12,
				12 * v.savingYears,
				v.requiredCapitalForPensionFromSavings,
				0,
				1
			);
		}

		return {
			fromIncome: v.pensionFromIncome,
			fromState: v.pensionFromState,
			missingIncome: v.missingIncome,
			requiredSavingsForPensionFromSavings: v.requiredSavingsForPensionFromSavings
		};
	},

	getPensionFromStateRates: function(years) {
		var sel = 0;
		for (var i = 0; i < this.pensionsFromStateRates.length; ++i) {
			if (years >= this.pensionsFromStateRates[i].years) sel = i; 
		}
		
		return this.pensionsFromStateRates[sel];
	},

	getContributionRate: function(years) {
		var sel = 0;
		for (var i = 0; i < this.contributionRates.length; ++i) {
			if (years >= this.contributionRates[i].years) sel = i; 
		}
		
		return this.contributionRates[sel];
	},

	getSkala44: function(income) {
		var sel = 0;
		for (var i = 0; i < this.skala44.length; ++i) {
			if (income >= this.skala44[i].income) sel = i; 
		}
		
		return this.skala44[sel];
	},

	//Calculates the future value of an investment based on a constant interest rate (Endvalue or expected payment from savings after n1 years)
	expectedCapitalFromSavings: function(i1, n1, mpr, pv, type) {
	/*
		* i1   - interest rate per month
		* n1   - number of periods (months)
		* mpr  - monthly savings ("Monatsprämie", monatlicher Sparbetrag)
		* pv   - present value (Einmalprämie, 0 in unserem Fall)
		* type - when the payments are due:
		*        0: end of the period, e.g. end of month (default)
		*        1: beginning of period
		* zz1  - Zinseszins (1+i)^n
		* sn   - q*(q^n-1)/(q-1) vorschüssig, resp. (q^n-1)/(q-1) nachschüssig
		*/
		var expectedCapitalFromSavings, zz1, q1, sn1;
		var q1 = 1 + i1;
		var zz1 = Math.pow(1 + i1, n1);
		var sn1 = q1 * type * (zz1-1) / (q1 - 1)
		if (i1) {
			expectedCapitalFromSavings = -1 * ((mpr * sn1) + pv * zz1);
		} else {
			expectedCapitalFromSavings = -1 * (mpr * n1 + pv);
		}
		return expectedCapitalFromSavings.toFixed(2);
	},

	//Calculates how much PensionFromSavings can be paid during a certain time (until 95 in our case) from a start value pv and an endvalue fv (0 in our case) with a contant interest rate (Annuität)
	pensionFromSavings: function(i2, n2, pv, fv, type) {
		/*
		* i2   - interest rate per month
		* n2   - number of periods (months)
		* pv   - present value
		* fv   - future value
		* type - when the payments are due:
		*        0: end of the period, e.g. end of month (default)
		*        1: beginning of period
		* zz2  - Zinseszins (1+i)^n
		* an   - q*(q^n-1)/(q-1)/q^n vorschüssig, resp. (q^n-1)/(q-1)/q^n nachschüssig (=sn/q^n)
		*/
		var pensionFromSavings, zz2, q2, an2;
		var q2 = 1 + i2;
		var zz2 = Math.pow(q2, n2);
		var an2 = (zz2-1) / (q2 - 1) / zz2		

		if (i2) {
			pensionFromSavings = -1 * ((pv - fv) / an2);
		} else {
			pensionFromSavings = -1 * ((pv - fv) / n2);
		}
		if (type === 1)
			pensionFromSavings /= q2;

		return pensionFromSavings;
	},

	//Calculates how much capital is required in order to get the missing pension from savings (until 95 in our case) with a certain rest capital (0 in our case) with a contant interest rate (Annuität)
	requiredCapitalForPensionFromSavings: function(i3, n3, mi, fv, type) {
		/*
		* i3   - interest rate per month
		* n3   - number of periods (months)
		* mi   - missing income
		* fv   - future value
		* type - when the payments are due:
		*        0: end of the period, e.g. end of month (default)
		*        1: beginning of period
		* zz3  - Zinseszins (1+i)^n
		*/
		var requiredCapitalForPensionFromSavings, zz3, q3, sn3;
		var q3 = 1 + i3;
		var zz3 = Math.pow(1 + i3, n3);
		var an3 = (zz3-1) / (q3 - 1) / zz3

		fv || (fv = 0);
		type || (type = 0);

		if (i3 === 0)
		 	return (mi * n3 + fv / q3)

		/*requiredCapitalForPensionFromSavings = - mi * (1 + i3*type) * (1 - zz3) / (i3 * (zz3 + fv));*/
		requiredCapitalForPensionFromSavings = mi * an3 + fv / q3;
	

		return requiredCapitalForPensionFromSavings;
	},

	//Calculates how much has to be saved monthly (there is presently no startcapital) in order to get the required capital for the wished pension from savings with a constant interest rate
	requiredSavingsForPensionFromSavings: function(i4, n4, fv, pv, type) {
		/*
		* i4   - interest rate per month
		* n4   - number of periods (months)
		* fv   - required capital for wished pension
		* pv   - start capital (=0 in our case)
		* type - when the payments are due:
		*        0: end of the period, e.g. end of month (default)
		*        1: beginning of period
		* zz4  - Zinseszins (1+i)^n
		*/
		var requiredSavingsForPensionFromSavings, zz4, q4, zz4, sn4;
		var q4 = 1 + i4;
		var zz4 = Math.pow(1 + i4, n4);
		var sn4 = q4 * type * (zz4-1) / (q4 - 1)

		if (i4) {
			requiredSavingsForPensionFromSavings = (fv / sn4 - pv);
		} else {
			requiredSavingsForPensionFromSavings = (fv / n4 - pv);
		}
		return requiredSavingsForPensionFromSavings.toFixed(2);


		/*if (i4) {
			requiredSavingsForPensionFromSavings = - (fv / ((1 + i4*type) * (1 - zz4) / i4) - pv * zz4);
		} else {
			requiredSavingsForPensionFromSavings = (fv - pv) / n4;
		}
		return requiredSavingsForPensionFromSavings.toFixed(2);*/
	}
};

window.Calculator = Calculator;

})();