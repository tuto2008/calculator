window.onload 			= function() {
	document.domain 	= "doris";

	var divTarget 		= document.getElementById('calculator'), parent = divTarget.parentNode,
	tempDiv 			= document.createElement('div');
	
	var lang 			= divTarget.getAttribute("data-lang");
	lang 				= lang === null ? '' : '?lang='+lang;

	var iframePath 		= 'calculator/iframe.php'+lang;

	tempDiv.innerHTML 	= '<iframe id="calculator" style="overflow:hidden" scrolling="no" src="'+iframePath+'" width="100%" height="0" frameborder="0"></iframe>'; 
	var input 			= tempDiv.childNodes[0];
	parent.replaceChild(input, divTarget);

	ajustar 			= function(altura){
		var iframeTarget = document.getElementById('calculator');
		iframeTarget.style.height = altura + "px";
	}
};