	var ani 			= {};
	ani.animations 		= [];
	ani.posi 			= 0;
	ani.dataBalls 		= [];

	var ballAnimation = function(obj){
		this.values = {
			target: 	obj,
			animation: 	true,
			speed: 		30
		};

		var v = this;

		this.values.target.animateSprite({
		    fps: this.values.speed,
		    animations: {
		    	up: 	[0,1,2,3,4,5,6,7,8,9],
		    	down: 	[9,8,7,6,5,4,3,2,1,0],
		        stop: 	[13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,20,20,20,19,19,18,18,17,17,16,16,15,15,14,14,13,13]
		    },
		    loop: false,
		    complete: function(){
		    	if(v.values.animation){
					v.staticBall();
					var animationType = $('#animationWrap').attr('class');
					var title = ani.dataBalls[obj.parent().index()].title;
					var value = ani.dataBalls[obj.parent().index()].val;
					obj.prepend( '<div class="layer"><h4>'+title+'</h4><p><span>'+currentCurrency+'</span> '+value+'</p></div>' );
					obj.find('h4').fadeIn(900);
					obj.find('p').slideDown(400);
		    	} 
		    	ajustar();
		    }
		});

		this.staticBall = function(){
			this.values.target
				.animateSprite('play','stop')
				.animateSprite('loop',true)
				.animateSprite('fps',9);
		}

		this.endBall = function(){
			var object = this;
			obj.find('h4').fadeOut(300);
			obj.find('p').slideUp(400,function(){
				obj.find('.layer').remove();
				object.values.animation = false;
				object.values.target
					.animateSprite('play','down')
					.animateSprite('loop',false)
					.animateSprite('fps',object.values.speed);
			});
		}
	}

	ani.startAnimation = function(btnType){
		if(ani.posi == 0){
			var group 	= $('.sprite');
			var plus 	= 0;
			if($('#animationWrap').attr('data-type') !== undefined){
				group 	= $('.ball');
				plus 	= 1;
				ani.posi 	= 1;
			} 
			$('#animationWrap').attr('data-type',btnType);

			$('#animationWrap').attr('class','').addClass(btnType);
			group.each(function(index){
				$(this).delay(100 * (index + plus)).show(function(){
					var name = 'ani.posi'+ ani.posi;
					$(this).attr('data-name',name);
					var current = $('#' + $(this).attr('id'));
					if(!ani.animations[name]){
						ani.animations[name] = new ballAnimation(current);
					} else {
						ani.animations[name].values.animation = true;
						current.animateSprite('play','up');
					}
					ani.posi++;
				});
			})
		} else {
			$('.sprite').each(function(index){
				var value 	= ani.dataBalls[index].val;
				var target 	= $(this).find('p');
				var text 	= '<span>'+currentCurrency+'</span> ' +value;
				target.fadeOut(200,function(){
					target.html(text).fadeIn(200);
				});
			}).promise().done( function(){ 
				ajustar();
			});
		}
		return false;
	}

	ani.changeType 		= function(btnType){
		var newtitle 	= ani.dataBalls[0].title;
		var newVal 		= ani.dataBalls[0].val;
		$('#bullet p').slideUp(400,function(){
			$('#bullet h4').html(newtitle);
			$('#bullet p').html(newVal).slideDown(400,function(){
				$('#animationWrap').attr('class',btnType);
				ani.startAnimation(btnType);
			});
		});
	}

	ani.removeAnimation = function(callback){
		var waitTime = $('#animationWrap').attr('data-type') === undefined ? 0 : 940;
		ani.endAnimations($('.sprite'));
		setTimeout(function() {
			$('#animationWrap').attr('class','').removeAttr('data-type');
			if (typeof callback === 'function'){
				ajustar();
				callback();
			}
		}, waitTime);
	}

	ani.endAnimations = function(type){
		if(ani.posi > 0){
			$(type.get().reverse()).each(function(index){
				$(this).delay(100 * index).show(function(){
					var name = $(this).attr('data-name');
					ani.animations[name].endBall();
				});
			});
			ani.posi = 0;
		} 
	}

	/* INTRO BALL ANIMATION */

	iBall 					= {};

	$(function(){
		iBall.tween;
		iBall.ball 				= $('#sphere-container');
		iBall.shadow 			= $('.shadow');
		iBall.isShow 			= false;

		var Sphere = function(img, noise, domId, width, height, callback) {

			var self = this;

			var app = new PIXI.Application(width, height, {backgroundColor : 0xffffff});
			var container = document.getElementById(domId);
			container.appendChild(app.view);

			this.distort = 15;
			this.speed = 5;

			var displacementTexture = PIXI.Sprite.fromImage(noise);
			displacementTexture.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
			var displacementFilter = new PIXI.filters.DisplacementFilter(displacementTexture, this.distort);
			
			var sphere = PIXI.Sprite.fromImage(img)
			sphere.anchor.set(0.5);
			sphere.x = app.renderer.width / 2;
			sphere.y = app.renderer.height / 2;

			displacementTexture.anchor.set(0.5);
			displacementTexture.x = app.renderer.width / 2;
			displacementTexture.y = app.renderer.height / 2;

			app.stage.addChild(displacementTexture);
			app.stage.addChild(sphere);

			sphere.filters = [displacementFilter];

			app.ticker.add(function(delta) {
				displacementFilter.scale.x = displacementFilter.scale.y = self.distort;
				displacementTexture.x += self.speed / delta;
			});

			setTimeout(function() {
				callback();
			}, 120);
			
		};

		iBall.distort = function(){
			if(typeof iBall.tween === 'object'){
				iBall.tween.kill();
			}
			iBall.s.distort = 15;
			iBall.s.speed = 5;
			iBall.tween = TweenMax.to(iBall.s, 0.4, {distort:50, speed:20, yoyo:true, repeat:1, ease:Quad.easeOut});
		} 

		iBall.start 			= function(){
			if(!iBall.isShow){
				iBall.s 			= new Sphere('assets/images/sphere.png', 'assets/images/noise.png', 'sphere-container', 512, 512, iBall.startAnimation);
			}
		}

		iBall.startAnimation 	= function(){
			iBall.isShow		= true;
			var aniBall 		= new TimelineMax({onComplete:iBall.gototo});
			var lm 				= iBall.getLeftPosition();

			aniBall.append(TweenMax.to(iBall.ball, 0.20, {top:-15,width:"83%",height:"65%",left:lm,ease:Quad.easeInOut}));
			aniBall.append(TweenMax.to(iBall.ball, 0.10, {top:0,width:"100%",height:"78%",left:0,ease:Quad.linear}));
			aniBall.insert(TweenMax.to(iBall.shadow, 0.30, {backgroundSize:'70%'}));
		}

		iBall.getLeftPosition  = function(){
			$w 			= $(window).width();
			if($w<=620){
				lm=10;
			} else {
				lm=30;
			}	
			return lm;
		}

		iBall.gototo 			= function(){
			iBall.aniBall 		= TweenMax.to(iBall.ball, 1.5, {top:14, repeat:-1, yoyo:true, ease:Quad.easeInOut});
			iBall.aniShadow 	= TweenMax.to(iBall.shadow, 1.5, {backgroundSize:'100%', repeat:-1, yoyo:true, ease:Quad.easeInOut});
		}

		iBall.endAnimation 		= function(){
			var ani1 			= new TimelineMax({
				onComplete:function(){
					iBall.destroy(processResults);
				}
			});
			var lm				= iBall.getLeftPosition();
			var altura 			= $('.ballAnimation').height() * 0.82;

			ani1.append(TweenMax.to(iBall.ball, 0.15, {top:-5,width:"83%",height:"65%",left:lm, ease:Quad.easeInOut}));
			ani1.append(TweenMax.to(iBall.ball, 0.25, {top:altura,width:0,height:0,left:"40%"}));
			ani1.insert(TweenMax.to(iBall.shadow, 0.40, {backgroundSize:'0%'}));
		}

		iBall.destroy = function(callback){
			$('#sphere-container canvas').remove();
			$('.ballAnimation').hide();
			$('.aniResults').show();
			iBall.isShow = false;
			callback();
		}

		$('#arrugar').click(iBall.distort);
		$('#test').keyup(iBall.distort);
		$('#salirBall').click(iBall.endAnimation);
		$('#entrarBall').click(iBall.start);
	});

	/* END INTRO BALL ANIMATION */