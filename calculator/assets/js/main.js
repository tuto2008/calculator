$w = "";
$ac = {}
$(window).resize(ajustar);
$(window).on('load',ajustar);
$(window).bind('orientationchange', ajustar);
$(function(){
	//document.domain = "vorsorge-mobiliar.ch"; 
	document.domain = TDOMAIN; 
	$('.number').mask('000’000', {reverse: true}).keyup(checkMax);
	$('.rangenumber').mask('0000', {reverse: true}).keyup(checkRange).focusout(checkRange);
	$('nav.type a').click(changeType);
	$('ul.gender li').click(changeGender);
	$('a#btnStart').click(gotoStep2);
	$('a#btnCalculate').click(getResults);
	$('#btnReset').click(resetForm);
	$('.required').keyup(checkRequired);
	$('#btnPDF').click(GeneratePdf);

	var prevFieldVal;
	$('.field').keyup(fieldsChange).focus(function() {
	    var $this = $(this);
	    prevFieldVal = $this.val();
	    $this.val('');
	}).blur(function() {
	    var $this = $(this);
	    if ($this.val().trim() == '') $this.val(prevFieldVal);
	});

	$('#btnTypes').click(MobileChange);

	startSliders();
	ajustar();
});

function MobileChange(){
	$('.typesContent').slideToggle(400);
	ajustar();
	return false;
}

function getResults(){
	if( !$(this).hasClass('disable')){
		$activetype = $('nav.type a.active').attr('data-type');
		$ac[$activetype] = true;
		trackEvent('goal-berechnen');
		closeIntroAni();
		if($('body').hasClass('mobile')){
			$('.typesContent').slideUp(400);
			$('.actionContent').slideDown(400);
		}
	}
	return false;
}

function fieldsChange(){
	if( $('#animationWrap').attr('data-type') !== undefined ){
		closeIntroAni();
	} else {
		iBall.distort();
	}
}

function closeIntroAni(){
	iBall.endAnimation();
}

function processResults(){
	var type 						= $('nav.type .active').attr('data-type');
	var monthlyNetIncome_type 		= (type == 'sparbetrag') ? $('#monthlyNetIncome1') : $('#monthlyNetIncome2');
	var PensionAge_type 			= (type == 'sparbetrag') ? $('#startPensionAge1') : $('#startPensionAge2');

	var monthlyNetIncome 			= monthlyNetIncome_type.val();
	monthlyNetIncome 				= deutchToNumber(monthlyNetIncome);

	var c 							= new Calculator();
	c.values.sex 					= getCurrentGender();
	c.values.age 					= getAge();
	c.values.monthlyBrutIncome 		= monthlyNetIncome;
	//c.values.monthlyNetIncome 		= monthlyNetIncome;
	c.values.startPensionAge 		= parseInt(PensionAge_type.val());

	var dataPdf 					= {};
	dataPdf.val1					= roundResultToDeutch(monthlyNetIncome);
	dataPdf.val3 					= c.values.startPensionAge;

	if(type == 'sparbetrag'){
		var monthlySavings 			= deutchToNumber($('#monthlySavings').val());
		//var monthlySavings 			= $('#monthlySavings').val();
		c.values.monthlySavings 	= monthlySavings;
		dataPdf.val2				= roundResultToDeutch(monthlySavings);
		$dataResult 				= c.getFromSavings();
		dataPdf.val4				= roundResultToDeutch($dataResult.monthlyIncome);
		dataPdf.val5				= roundResultToDeutch($dataResult.fromState);
		dataPdf.val6				= roundResultToDeutch($dataResult.fromIncome);
		dataPdf.val7				= roundResultToDeutch($dataResult.fromSavings);
	} else {
		var wishedPension 			= deutchToNumber($('#monatsrente').val());
		c.values.wishedPension 		= wishedPension;
		dataPdf.val2				= roundResultToDeutch(wishedPension);
		$dataResult 				= c.getWished();
		dataPdf.val4				= roundResultToDeutch($dataResult.requiredSavingsForPensionFromSavings);
		dataPdf.val5				= roundResultToDeutch($dataResult.fromState);
		dataPdf.val6				= roundResultToDeutch($dataResult.fromIncome);
		dataPdf.val7				= roundResultToDeutch($dataResult.missingIncome);
	}

	drawResults($dataResult,type);
	$('#btnPDF').removeClass('disable');
	$('#resultType').val(type);
	$('#resultValues').val(JSON.stringify(dataPdf));	
}


function drawResults(data,type){
	switch(type){
		case 'sparbetrag':
			ani.dataBalls 		= [
			    {"title": resultTexts[type][0] , "val": roundResultToDeutch(data.monthlyIncome,'')},
				{"title": resultTexts[type][1] , "val": roundResultToDeutch(data.fromState,'')},
				{"title": resultTexts[type][2] , "val": roundResultToDeutch(data.fromIncome,'')},
				{"title": resultTexts[type][3] , "val": roundResultToDeutch(data.fromSavings,'')}
			];
		break;

		default:
			ani.dataBalls 		= [
			    {"title": resultTexts[type][0] , "val": roundResultToDeutch(data.requiredSavingsForPensionFromSavings,'')},
				{"title": resultTexts[type][1] , "val": roundResultToDeutch(data.fromState,'')},
				{"title": resultTexts[type][2] , "val": roundResultToDeutch(data.fromIncome,'')},
				{"title": resultTexts[type][3] , "val": roundResultToDeutch(data.missingIncome,'')}
			];
		break;
	}
	ani.startAnimation(type);
}

function roundResultToDeutch(number,end){
	end 	= typeof end !== 'undefined' ? end : '.–';
	number 	= number == 'Infinity' ? 0 : number;
	return numberToDeuch(Math.round(number)) + end;
}

function getAge(){
	var today 		= new Date();
	var month 		= today.getMonth() + 1;
	month 			= month<10 ? '0'+month : month;
	var day 		= today.getDate();
	day 			= day<10 ? '0'+day : day;
	var year 		= $('#yearField').val() + '/' + month + '/' + day;

    var birthDate 	= new Date(year);
    var age 		= today.getFullYear() - birthDate.getFullYear();
    var m 			= today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return parseInt(age);
}

function checkRequired(){
	$disabled = false;
	$('.typeTab.active .required').each(function(){
		if($(this).val() == 0 || $(this).val() == ''){
			$disabled = true;
		}
	});
	if(!$disabled){
		$('#btnCalculate').removeClass('disable');
	} else {
		$('#btnCalculate').addClass('disable');
	}
}

function trackEvent(e){
	dataLayer.push({'event': e});
}

function gotoStep2(){
	if(checkStep1()){
		trackEvent('goal-event-start');
		$('#calculatorContent').attr('class','step_02');
		$('.ballAnimation').show();
		$('.aniResults').hide();
		iBall.start();
		ajustar();
	}
	return false;
}

function checkStep1(){
	$check = false;
	$('ul.gender input').each(function(){
		if($(this).prop("checked")){
			$check = true;
		}
	});
	return $check;
}

function updatePensionalter(){
	$('input.pensionalter').each(function(){
		var t = $(this).closest('label.sliderWrap').find('.slider');
		t.slider('value', $(this).val());
	});
}

function getCurrentGender(){
	$check = false;
	$('ul.gender input').each(function(){
		if($(this).prop("checked")){
			$check = $(this).val();
		}
	});
	return $check;
}

function changeGender(){
	$('ul.gender li').removeClass('active');
	$target = $(this).find('input');
	$target.prop( "checked", true );
	$(this).addClass('active');
	$('#btnStart').removeClass('disable');
	return false;
}

function resetForm(){
	trackEvent('resets-pro-sessions');
	ani.removeAnimation(resetFields);
	return false;
}

function resetFields(){
	$('#calculatorForm').get(0).reset();
	$( ".slider" ).slider("destroy");
	$('.result p').html('');
	$('#calculatorContent').attr('class','step_01');
	$('ul.gender li.active').removeClass('active');
	$('#btnStart, #btnPDF').addClass('disable');
	$('.typesContent').show();
	$('.actionContent').hide();
	startSliders();
	resetTypeTab( $('nav.type a.option_01'), $('#type_01'));
}

function startSliders(){
	$( "label.sliderWrap" ).each(createSlider);
}

function createSlider(){
	$dS 		= $(this).find('.slider');
	var r 		= $dS.attr('data-range');
	var v 		= parseInt($dS.attr('data-value'));
	var min 	= parseInt($dS.attr('data-min'));
	var max 	= parseInt($dS.attr('data-max'));
	var step 	= $dS.attr("data-step") ? parseInt($dS.attr("data-step")) : 1;

    $dS.slider({
		range: 	r,
		value: 	v,
		min: 	min,
		max: 	max,
		step: 	step,
		slide: 	function( event, ui ) {
			$dF = $(this).closest('label.sliderWrap').find('.sliderValue');
			var fieldValue = $dF.hasClass('currency') ? numberToDeuch(ui.value) : ui.value;
			$dF.val( fieldValue );

			fieldsChange();

			if( $dF.hasClass('pensionalter') ){
				$('input.pensionalter').val(fieldValue);
				updatePensionalter();
			}
		}
    });

    $(this).find('.sliderValue').val(v);
}

function numberToDeuch(number){
	if(number > 0){
		var integer 	= parseInt(number/1000);
		var decimal 	= number - (integer*1000);
		var pad 		= "000";
		var decimal 	= (pad+decimal).slice(-pad.length);
		if(integer >= 1){
			return integer + "’" + decimal;
		} else {
			decimal = parseInt(decimal) < 100 ? parseInt(decimal) : decimal; 
			return decimal;
		}
	} else {
		return 0;
	}
}

function deutchToNumber(string){
	return parseInt(string.replace("’",""));
}

function checkMax(){
	var maxValue = $(this).attr('data-max');
	if(maxValue){
		var currentValue = deutchToNumber($(this).val());
		if( currentValue > parseInt(maxValue) ){
			$(this).val( numberToDeuch(maxValue) );
		}
	}
	if($(this).hasClass('monatseinkommen')){
		$('input.monatseinkommen').val($(this).val());
	}
}

function checkRange(){
	if ($(this).attr('data-type') == 'deutch'){
		$(this).val( deutchToNumber( $(this).val() ));
	}
	
	var maxValue 	= $(this).attr('data-max');
	var minValue 	= $(this).attr('data-min');
	var currValue 	= $(this).val();
	//Check if field is in focus or not to calculate minimun an maximun values
	var conditional = $(this).is(":focus") ? currValue.length == 4 : 1 == 1;
	if( currValue > parseInt(maxValue) ){
		$(this).val( maxValue );
	}
	if( currValue < parseInt(minValue) && conditional){
		$(this).val( minValue );
	}

	if ($(this).attr('data-type') == 'deutch'){
		$(this).val( numberToDeuch( $(this).val() ));
	}
}

function updateBoth(obj){
	alert(obj.val());
}

function changeType(){
	changeTypeFunction( $(this) , $($(this).attr('href')) );
	return false;
}

function changeTypeFunction(o,$target){
	if(!o.hasClass('active')){
		ani.removeAnimation(startNewAnimation);
		resetTypeTab(o,$target);
		if($('body').hasClass('mobile')){
			$('.actionContent').slideUp(400);
		}
	}
}

function startNewAnimation(){
	if( !$('#btnCalculate').hasClass('disable')){
		getResults();
	}
	ajustar();
}

function resetTypeTab(o,$target){
	$('nav.type a').removeClass('active');
	o.addClass('active');
	$('.contentBlock').hide();
	$('.typeTab.active').removeClass('active');
	$target.addClass('active');
	$target.find('.contentBlock').each(function(){
		$(this).slideDown(400,function(){
			ajustar();
		});
	});
	$('#btnPDF').addClass('disable');
	checkRequired();
}

function detectIframe(){
	if (window.self !== window.top) {
		return true;
	} else {
		return false;
	}
}

function GeneratePdf(){
	if(!$(this).hasClass('disable')){
		trackEvent('goal-pdf');
		$('form#pdfGenerator').submit();
	}
	return false;
}

function ajustar(){
	if(detectIframe()){
		$h = $('#wrapHeight').outerHeight();
		$('body').attr('data-height',$h);
		window.top.ajustar($h);
	}
	$w 			= $(window).width();
	if($w>500){
		$('body').addClass('desktop').removeClass('mobile');
	} else {
		$('body').removeClass('desktop').addClass('mobile')	;
	}
}