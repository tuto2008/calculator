(function($) {
    $.fn.comboBox 	= function(options){
    	if(options.action == "reset"){
    		reset($(this));
    	} else {
    		convert($(this),options);
    	}

    	function reset(obj){
    		var items 	= obj.find('.mCSB_container label');
    		var first 	= items.eq(0);
    		var valor 	= first.find('input').val();
    		items.removeClass('js-open');
    		first.addClass('js-open');
    		$('.js-value').text(valor);
    		$('#yearField').val(valor);
    	}

	    function convert(obj,options){
			var tpl 	= '<label class="item{active}" for="{nameID}">{nameText}<input type="radio" id="{nameID}" name="{name}" value="{nameText}"/></label>';
			var cont 	= 0;
			var html 	= '';
			obj.wrap('<div id="'+options.name+'" class="dropp"></div>');
			var wrap = obj.closest('.dropp');
			
			if(obj.is('[data-items]')){
				var str = obj.attr('data-items');
				var data = str.split(",");
			}

			obj.before('<div class="dropp-header"><span class="dropp-header__title js-value">'+data[0]+'</span><strong class="dropp-header__btn js-dropp-action"></strong></div><div class="dropp-body"><div class="scroll"></div></div>');

			$.each(data, function(index, value) {
				var active = (index==0) ? ' js-open': '';
				html    = tpl.replace(/{nameID}/g,'opt_'+('000'+index).slice(-3));
				html    = html.replace(/{nameText}/g,value);
				html    = html.replace('{name}',options.name);
				html    = html.replace('{active}',active);
				wrap.find('.scroll').append(html);
			}); 
			wrap.after('<input type="hidden" id="yearField" value="'+data[0]+'">');
			obj.remove();

			$(".scroll").mCustomScrollbar();

			$('.dropp-header').on('click',function(e){  
				$('.js-dropp-action').toggleClass('js-open'); 
				$('.js-dropp-action').parent().next('.dropp-body').toggleClass('js-open');
			});

			$('.dropp label.item').on('click',function(){ 
				$(this).addClass('js-open').siblings().removeClass('js-open');
				$('.dropp-body,.js-dropp-acti.siblingson').removeClass('js-open');
			});
			$('.dropp input').on('change',function(){ 
				var value = $(this).val();
				$('.js-value').text(value);
				$('.dropp-header__btn').removeClass('js-open');
				$('#yearField').val(value);
			});
		}
	};
})(jQuery);