# README #

This is the first integration of calculator

### Change the cross domain variable (document.domain) ###

* Open the file "calculator/assets/js/calculator.js"
* replace "doris.roomsetenta.es" with the final domain and save the file.
* Now open the file "calculator/assets/js/main.js" and repeat the step 2

### Change the app folder name ###

If you need to change the name of the app folder, take the following steps:

* Rename folder call "calculator" with the new name.
* Open the file "calculator/assets/js/calculator.js"
* Change the variable (iframePath) with the new path to file iframe.php.

### How to use ###

Here are the steps you need follow to integrate into your html file:

* Open the file "index.html".
* Copy th lines 11,12,13 and 14.
* Insert in your final/test html file.
* Replace the script path to new file pah if you have changed app folder name.